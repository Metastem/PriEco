<?php
if (strpos($purl, 'weather') !== false) {
   echo '<div class="output"style="margin-bottom: 10px;border-radius: 20px;padding: 10px;">';

    $weather .= '<div style="float:left;"><img src="https://openweathermap.org/img/wn/'.$OpenWeatherObj['weather'][0]['icon'].'@2x.png"><br>';
    $weather .= 'Temperature: '.$OpenWeatherObj['main']['temp'].' K<br>';
    $weather .= 'Feels Like: '.$OpenWeatherObj['main']['feels_like'].' K</div>';
    
    $weather .= '<div style="float:right; margin-right:20%;"><h2>'.$OpenWeatherObj['name'].'</h2>';
    $weather .= ucfirst($OpenWeatherObj['weather'][0]['description']).'<br>';
    $weather .= 'Humidity: '.$OpenWeatherObj['main']['humidity'].'%<br>';
    $weather .= 'Pressure: '.$OpenWeatherObj['main']['pressure'].'P<br>';
    $weather .= 'Wind: '.$OpenWeatherObj['wind']['speed'].'m/s';
   $weather .= '<br><br><a href ="https://openweathermap.org/"><p>Data from OpenWeatherMap</p></a>';
    $weather .= '</div>';
    echo $weather.'<br><br></div><br><br><br>';
}