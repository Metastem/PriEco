<?php
function wiki($Wiki, $wikiOther, $lang, $data, $mysql, $hideQueryCopy)
{
    $answer = '';
    $ansImg = '';

$content_data = $wikiOther["query"]["pages"][0]["revisions"][0]["content"];
$lines = explode("\n", $content_data);
$values = array();
$in_infobox = false;

$tmp = ($lang == 'all') ? 'en' : $lang;

foreach ($lines as $line) {
if (preg_match("/^\{\{Infobox/", $line)) {
    $in_infobox = true;
}
elseif (preg_match("/^\}\}$/", $line)) {
    $in_infobox = false;
}
elseif ($in_infobox && preg_match("/^\|\s*(\w+)\s*=\s*(.*)$/", $line, $matches)) {
    $key = $matches[1];
    $value = $matches[2];

    $values[$key] = $value;
}
}
    //Wikipedias
        if (strpos(json_encode($Wiki['query']['pages']), '{"-1":{') === false && isset($Wiki)) {
            $WikiId = get_string_betweens(json_encode($Wiki), '"pages":{"', '":{"pageid":');
            $wikidata = $Wiki['query']['pages'][$WikiId];

            if (strpos($wikidata['extract'], 'may refer to:') === false && isset($wikidata)) {
                $answer .= '<div class="answer" id="answer"><h2>' . $wikidata['title'] . '</h2><br>';

                $digest = md5(str_replace(' ','_',$values['image']));
                $urlencfile =  urlencode(str_replace(' ','_',$values['image']));
                $imgwidth = 220;
                $folder = $digest[0] . '/' . $digest[0] . $digest[1] . '/' . $urlencfile . '/' . $imgwidth . 'px-' . $urlencfile;

                    if(!isset($_COOKIE['datasave']) && $urlencfile != ''){
                        $wikiImgForSim = '/Controller/functions/proxy.php?q=https://upload.wikimedia.org/wikipedia/commons/thumb/' . $folder;
                        if(str_ends_with($wikiImgForSim,'.svg') ){$wikiImgForSim .= '.webp';}
                        $answer .= '<img src="' . $wikiImgForSim . '" alt="‎" style="max-width: 50%;border-radius: 30px;max-height: 200px;height:auto;width: auto;">';
                    }
                $digest = md5(str_replace(' ','_',$values['signature']));
                $urlencfile =  urlencode(str_replace(' ','_',$values['signature']));
                $imgwidth = 220;
                $folder = $digest[0] . '/' . $digest[0] . $digest[1] . '/' . $urlencfile . '/' . $imgwidth . 'px-' . $urlencfile.'.png';
                    if(!isset($_COOKIE['datasave']) && $urlencfile != ''){
                            $answer .= '<img class="signWiki"src="/Controller/functions/proxy.php?q=https://upload.wikimedia.org/wikipedia/commons/thumb/' . $folder . '" alt="‎" style="max-width: 50%;border-radius: 30px;height: 200px;width: auto;">';
                        }
                $answer .= '<br><br><p style="font-weight: bold;font-size: 12px;">Description:</p><p>' . substr($wikidata['extract'], 0, 500) . '...' . '<a style="color: #3391ff;text-decoration: none;" href="https://'.$tmp.'.wikipedia.org/wiki/' . $wikidata['title'] . '" target="_blank">Wikipedia</a></p>';

                $bdate = explode('|',str_replace('{{Birth date and age','',str_replace('}}','',$values['birth_date'])));
                $bdate = $bdate[3].'.'.$bdate[2].'.'.$bdate[1];
                $bplace = str_replace('[[','',str_replace(']]','',$values['birth_place']));
                
                if(isset($values['birth_name']) || $bdate!='..' || $bplace != ''){$answer.='<br><br><p style="font-weight: bold;font-size: 12px;">Born</p>';}
                if(isset($values['birth_name'])){$answer .= '<p>Name: '.$values['birth_name'].'</p>';}
                if($bdate!='..'){$answer .= '<p>Date: '.$bdate.'</p>';}
                if($bplace != ''){$answer .= '<p>Place: '.$bplace.'</p><br>';}

                $toccupation = explode('|',str_replace('{{hlist','',str_replace('}}','',$values['occupation']))); 
                unset($toccupation[0]);
                $occupation ='';
                foreach($toccupation as &$oc){
                    $occupation.=$oc.'<br>';
                }
                if($occupation != '' && $occupation != '<br>'){
                $answer .= '<p style="font-weight: bold;font-size: 12px;">Occupations</p><p>'.$occupation.'</p><br>';
                }
                if($values['years_active']){
                $answer .= '<p style="font-weight: bold;font-size: 12px;">Years active</p><p>'.$values['years_active'].'</p>';
                }
            }
    if($answer!=''){
        $answer .= '<br><p style="font-weight: bold;font-size: 12px;">Profiles</p>';
        $answer .= '<a href="https://'.$tmp.'.wikipedia.org/wiki/' . $wikidata['title'].'"'; if (isset($_COOKIE['new'])) {
            $answer .= 'target="_blank"';
        } $answer .='><button class="socialBtn"><div>';
        if(!isset($_COOKIE['datasave'])) {$answer.='<img alt="‎" src="./View/icon/profiles/wiki.svg" class="profileIcon">';}
        $answer .= '<p>Wikipedia</p></div></button></a>';

        $twitter='';
        $facebook='';
        $imdb='';
        $tomato='';
        $spotify='';
        $apple='';
        if(!$mysql && gettype($data) == 'string'){
            $data = explode(' ', $data);
            $twitter = $data[0];
            $facebook = $data[1];
            $imdb = $data[2];
            $tomato = $data[3];
            $spotify = $data[4];
            $apple = $data[5];
        }
        else{
            if(isset($data['Infobox']['content'])){
            foreach($data['Infobox']['content'] as &$item){
                if($item['data_type'] == 'twitter_profile'){
                    $twitter = $item['value'];
                }
                if($item['data_type'] == 'facebook_profile'){
                    $facebook = $item['value'];
                }
                if($item['data_type'] == 'imdb_id'){
                    $imdb = $item['value'];
                }
                if($item['data_type'] == 'rotten_tomatoes'){
                    $tomato = $item['value'];
                }
                if($item['data_type'] == 'spotify_artist_id'){
                    $spotify = $item['value'];
                }
                if($item['data_type'] == 'itunes_artist_id'){
                    $apple = $item['value'];
                }
            }
        }
        }

        if($twitter != ''){
            $answer .= '<a href="https://twitter.com/'.$twitter.'"'; if (isset($_COOKIE['new'])) {
                $answer .= 'target="_blank"';
            } $answer .='><button class="socialBtn"><div>';
            if(!isset($_COOKIE['datasave'])) {$answer.='<img alt="‎" src="./View/icon/profiles/twitterlogo.svg" class="profileIcon">';}
            $answer .= '<p>Twitter</p></div></button></a>';
        }

        if($facebook != ''){
            $answer .= '<a href="https://www.facebook.com/'.$facebook.'"'; if (isset($_COOKIE['new'])) {
                $answer .= 'target="_blank"';
            } $answer .='><button class="socialBtn"><div>';
            if(!isset($_COOKIE['datasave'])) {$answer.='<img alt="‎" src="./View/icon/profiles/facebook.svg" class="profileIcon">';}
            $answer .= '<p>Facebook</p></div></button></a>';
        }
            
        if($imdb != ''){
            $answer .= '<a href="https://www.imdb.com/name/'.$imdb.'"'; if (isset($_COOKIE['new'])) {
                $answer .= 'target="_blank"';
            } $answer .='><button class="socialBtn"><div>';
            if(!isset($_COOKIE['datasave'])) {$answer.='<img alt="‎" src="./View/icon/profiles/imdb.svg" class="profileIcon">';}
            $answer .= '<p>IMDb</p></div></button></a>';
        }
        if($tomato != ''){
            $answer .= '<a href="https://www.rottentomatoes.com/'.$tomato.'"'; if (isset($_COOKIE['new'])) {
                $answer .= 'target="_blank"';
            } $answer .='><button class="socialBtn"><div>';
            if(!isset($_COOKIE['datasave'])) {$answer.='<img alt="‎" src="./View/icon/profiles/tomato.svg" class="profileIcon">';}
            $answer .= '<p>Rotten Tomatoes</p></div></button></a>';
            }

        if($spotify != ''){
            $answer .= '<a href="https://open.spotify.com/artist/'.$spotify.'"'; if (isset($_COOKIE['new'])) {
                $answer .= 'target="_blank"';
            } $answer .='><button class="socialBtn"><div>';
            if(!isset($_COOKIE['datasave'])) {$answer.='<img alt="‎" src="./View/icon/profiles/spotify.svg" class="profileIcon">';}
            $answer .= '<p>Spotify</p></div></button></a>';
        }

        if($apple != ''){
            $answer .= '<a href="https://music.apple.com/artist/'.$apple.'"'; if (isset($_COOKIE['new'])) {
                $answer .= 'target="_blank"';
            } $answer .='><button class="socialBtn"><div>';
            if(!isset($_COOKIE['datasave'])) {$answer.='<img alt="‎" src="./View/icon/profiles/apple.svg" class="profileIcon">';}
            $answer .= '<p>Apple Music</p></div></button></a>';
        }
    
    $answer .= '<br>'. $hideQueryCopy .'</div>';// Place for ad after <br>
   
    $ret[] = $answer;
    $ret[1] = $wikiImgForSim;
    return $ret;
    }
}

}