<?php
function search_reddit($results) {
        $rPrint = false;
        $conversations = '';
        $conversations.='<p style="float: left;
        clear: left;font-weight: bold;
        font-size: 16px;
        margin-left: calc(9vw + 20px);
        margin-top: 15px;padding-bottom: 10px;color:#616366;">💬 Discussions</p>

        <div class="output" style="border-radius: 20px;margin-bottom:15px;" id="output">';
        foreach($results['data']['children'] as &$res){        
            $rPrint = true;
            $conversations.='<div style="width:100%;height: 112px;">';   
            if(filter_var($res['data']['thumbnail'], FILTER_VALIDATE_URL)){
                if(!isset($_COOKIE['datasave'])) {
            $conversations.= '<img loading="lazy" alt="‎" src="/Controller/functions/proxy.php?q='. $res['data']['thumbnail']. '" class="OutSideImg">';
                }
            }
            if(!isset($_COOKIE['datasave'])) {
            $conversations.='<img class="Outfavicon" alt="‎" loading="lazy" src="/View/img/reddit.webp">  ';
            }          
           $conversations .=' <a ';
            if (isset($_COOKIE['new'])) {
                $conversations.='target="_blank"';
            }
            $conversations.= 'href="https://www.reddit.com'. $res['data']['permalink']. '" style="padding-top:unset;">';
            $conversations.= '<p class="OutTitle">'.substr($res['data']['title'], 0, 50). '...</p></a>
            <section style="display:inline;color:#747684;font-size:12px;">r/'. $res['data']['subreddit'].' ⋮ '.$res['data']['num_comments'].' Comments ⋮ '.$res['data']['ups'].' Votes ⋮ '.$res['data']['upvote_ratio']*100 .'% Upvoted ⋮ Author: <p style="font-weight:bold;display:inline;">'.$res['data']['author'].'</p></section>
            <p class="snippet">'.  substr($res['data']['selftext'], 0, 100). '...</p>
            ';
            $conversations.='</div>';
        }
        $conversations.='</div>';
        if($rPrint){
        return $conversations;
        }
    }