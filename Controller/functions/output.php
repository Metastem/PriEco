<!--  SPDX-FileCopyrightText: 2022, 2022-2022 Roman  Láncoš <jojoyou@jojoyou.org> -->
<!-- -->
<!--  SPDX-License-Identifier: AGPL-3.0-or-later -->

<?php
$Bpurl = urlencode($purl);

if ($type != 'image' and $type != 'video' and $type != 'news') {
    //Get string between characters
    function get_string_betweens($string, $start, $end)
    {
        $string = ' ' . $string;
        $ini = strpos($string, $start);
        if ($ini === 0) return '';
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return substr($string, $ini, $len);
    }

    include('Controller/functions/add.php');
    
    //Initial call
    if(!$dev){
        $ImpProfiles = false;
        $ImpGoogle = true;
        
        //Profiles from PriEco
        $name = mysqli_real_escape_string($conn, strtolower($purl));  
        $sql = "SELECT * FROM `profiles` WHERE `Name` = '$name'";
        $tmp = $conn->query($sql);
        $tmp = $tmp->fetch_assoc();

        $i=1;
        $ddgObj='';
        foreach($tmp as &$ddg){
            if($i>2){
                $ddgObj .= $ddg. ' ';
            }
            ++$i;
        }

        //Cached Google results
        $obj = '';
        if(!isset($_COOKIE['safe']) && !isset($_COOKIE['time'])){
        if(!isset($loc)){
            $loc = 'all';
        }
        if(!isset($lang)){
            $lang = 'all';
        }
        $sql = "SELECT * FROM `googleCache` WHERE `query` = '$name' AND `loc` = '$loc' AND `lang` = '$lang'";
        $tmp = $conn->query($sql);
        $tmp = $tmp->fetch_assoc();

        $obj = isset($tmp['results']) ? $tmp['results'] : '';

        if($obj != ''){
            if($tmp['official'] == 1){
            $ImpGoogle = false;
            $obj = json_decode($obj, true);
            }
            else{
                $ImpGoogle = false;
                $g2obj = json_decode($obj, true);
                $obj = '';
            }
        }
    
    }
        // Initialize multi-curl handle
$multiHandle = curl_multi_init();

// Initialize curl handles for each request
$curlHandles = array();
$curlCount = 0;
// Request 1: RedditFile
$ch1 = curl_init($RedditFile);
curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch1, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.13 (KHTML, like Gecko) Chrome/0.A.B.C Safari/525.13');
curl_setopt($ch1, CURLOPT_CONNECTTIMEOUT, 5);
$curlHandles[] = $ch1;
if(count($curlHandles) > $curlCount){
    $redditOn = true;
    $curlCount = count($curlHandles);
}

// Request 2: DdgFile
if($ddgObj == ''){
    $ImpProfiles = true;
    $ch2 = curl_init($DdgFile);
    curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch2, CURLOPT_HTTPHEADER, array(
        'Origin: https://ac.duckduckgo.com',
        'Referer: https://ac.duckduckgo.com/',
        'Accept-Language: en-US,en;q=0.9'
    ));
    curl_setopt($ch2, CURLOPT_CONNECTTIMEOUT, 5);
    curl_setopt($ch2, CURLOPT_FOLLOWLOCATION, true);
    $curlHandles[] = $ch2;
    if(count($curlHandles) > $curlCount){
        $ddgOn = true;
        $curlCount = count($curlHandles);
    }
}

// Request 3: Googlefile, Brave or Qwant
if($obj == '' && !isset($g2obj)){
    if(!file_exists('disGoogle.txt')){
        $ch3 = curl_init($Googlefile);
    }
    else{
        $ch3 = curl_init('https://librex.jojoyou3.repl.co/api.php?p=0&t=0&q='.$Bpurl);
        $tmp = $lang;
        if($lang = 'all'){$tmp = 'en';}
        $cookies = 'google_language_results='.$tmp.';'; 
        curl_setopt($ch3, CURLOPT_COOKIE, $cookies);
    }
    curl_setopt($ch3, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch3, CURLOPT_CONNECTTIMEOUT, 5);
    $curlHandles[] = $ch3;
    if(count($curlHandles) > $curlCount){
        $searchOn = true;
        $curlCount = count($curlHandles);
    }
}

//Related searches
if (!isset($_COOKIE['DisSugges'])) {
    $relatedCh = curl_init('https://cors-anywhere.jojoyou3.repl.co/ac.duckduckgo.com/ac/?type=list&callback=jsonCallback&_=1600956892202&q='.$Bpurl);
    curl_setopt($relatedCh, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($relatedCh, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.13 (KHTML, like Gecko) Chrome/0.A.B.C Safari/525.13');
    curl_setopt($relatedCh, CURLOPT_CONNECTTIMEOUT, 5);
    curl_setopt($relatedCh, CURLOPT_HTTPHEADER, array(
    'Origin: https://ac.duckduckgo.com'
));
   $curlHandles[] = $relatedCh;
   if(count($curlHandles) > $curlCount){
    $relatedOn = true;
    $curlCount = count($curlHandles);
}
}

$api_endpoint = 'https://en.wikipedia.org/w/api.php';
$page_title = str_replace(' ','_',ucwords($purl));
$params = array(
    "action" => "query",
    "format" => "json",
    "titles" => $page_title,
    "prop" => "revisions",
    "rvprop" => "content",
    "formatversion" => 2
);
$request_url = $api_endpoint . '?' . http_build_query($params);


//Wikipedia
$wikipediaOtherCh = curl_init($request_url);
curl_setopt($wikipediaOtherCh, CURLOPT_RETURNTRANSFER, true);
curl_setopt($wikipediaOtherCh, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.13 (KHTML, like Gecko) Chrome/0.A.B.C Safari/525.13');
curl_setopt($wikipediaOtherCh, CURLOPT_CONNECTTIMEOUT, 5);
$curlHandles[] = $wikipediaOtherCh;
if(count($curlHandles) > $curlCount){
    $wikipediaOtherOn = true;
    $curlCount = count($curlHandles);
}

$tmp = $_COOKIE['Language'];
    if($tmp == 'all' or $tmp == null){
        $tmp = 'en';
    }
$wikiCh = curl_init('https://'.$tmp.'.wikipedia.org/w/api.php?format=json&action=query&origin=*&prop=extracts&exintro&explaintext&redirects=1&titles=' . $Bpurl);
curl_setopt($wikiCh, CURLOPT_RETURNTRANSFER, true);
curl_setopt($wikiCh, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.13 (KHTML, like Gecko) Chrome/0.A.B.C Safari/525.13');
curl_setopt($wikiCh, CURLOPT_CONNECTTIMEOUT, 5);
$curlHandles[] = $wikiCh;
if(count($curlHandles) > $curlCount){
    $wikiOn = true;
    $curlCount = count($curlHandles);
}
//Define
if (isset($defWords)) {
    $defCh = curl_init($WordnikFile);
    curl_setopt($defCh, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($defCh, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.13 (KHTML, like Gecko) Chrome/0.A.B.C Safari/525.13');
    curl_setopt($defCh, CURLOPT_CONNECTTIMEOUT, 5);
      $curlHandles[] = $defCh;
  if(count($curlHandles) > $curlCount){
    $defOn = true;
    $curlCount = count($curlHandles);
    }
}
//Weather
if (strpos($purl, 'weather') !== false) {
    $weatherCh = curl_init($OpenWeatherFile);
    curl_setopt($weatherCh, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($weatherCh, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.13 (KHTML, like Gecko) Chrome/0.A.B.C Safari/525.13');
    curl_setopt($weatherCh, CURLOPT_CONNECTTIMEOUT, 5);
      $curlHandles[] = $weatherCh;
  if(count($curlHandles) > $curlCount){
    $weatherOn = true;
    $curlCount = count($curlHandles);
    }
}
// Execute multi-curl requests
foreach ($curlHandles as $handle) {
    curl_multi_add_handle($multiHandle, $handle);
}

$running = null;
do {
    curl_multi_exec($multiHandle, $running);
    curl_multi_select($multiHandle);
} while ($running > 0);

// Close the multi-curl handle
curl_multi_close($multiHandle);


// Get response content and error codes for each request
foreach ($curlHandles as $handle) {
    if (curl_error($handle) === '') {
        $response = curl_multi_getcontent($handle);

        if(isset($redditOn) && !isset($redditObj)){$redditObj = json_decode($response, true);}
        elseif(isset($ddgOn) && $ddgObj == ''){$ddgObj = json_decode($response, true);}
        elseif(isset($searchOn) && $obj == '' && !isset($g2obj)){
            if (!file_exists('disGoogle.txt')) {$obj = json_decode($response, true);}
            else{$g2obj = json_decode($response, true);}
        }
        elseif(isset($relatedOn) && !isset($related)){$related = json_decode($response, true);}
        elseif(isset($wikipediaOtherOn) && !isset($wikiOther)){$wikiOther = json_decode($response, true);}
        elseif(isset($wikiOn) && !isset($Wiki)){$Wiki = json_decode($response, true);}
        elseif(isset($defOn) && !isset($WordnikObj)){$WordnikObj = json_decode($response, true);}
        elseif(isset($weatherOn) && !isset($OpenWeatherObj)){$OpenWeatherObj = json_decode($response, true);}
    }    
    curl_multi_remove_handle($multiHandle, $handle);
    }
curl_multi_close($multiHandle);
}
else{
    $obj = json_decode(file_get_contents($Googlefile), true);
    $redditObj = json_decode(file_get_contents($RedditFile),true);
    $ddgObj = json_decode(file_get_contents($DdgFile),true);
    $related = json_decode(file_get_contents('./Controller/dev/sug.json'), true);
}

    //Engines
    include 'engines/web/google.php';
    include 'engines/web/google2.php';
    include 'engines/web/qwant.php';
    include 'engines/web/brave.php';
    include 'engines/web/promo.php';
    include 'engines/web/prieco.php';
    //Addons
    include 'addons/wiki.php';
    include 'addons/hideQuery.php';
    include 'addons/reddit.php';
    include 'addons/related.php';
    include 'addons/etsy.php';

    include 'addons/small/ip.php';
    include 'addons/small/define.php';
    include 'addons/small/weather.php';
    //include 'addons/small/didyoumean.php';
    
    //Next page
    function nextPage($purl,$page){
        if($page > 0){
            echo '<div class="nextPage" style="margin-left: calc(9vw + (clamp(552px, 675px, 90vw) - 210px) / 2);">';
            if(!isset($_COOKIE['hQuery'])){
            echo '<a href="/?q=',htmlspecialchars($purl, ENT_QUOTES | ENT_HTML5, 'UTF-8'),'&page=',$page-1,'">';
            }
            else{
                echo '<form method="POST" action="" style="display:inline;">
                <input type="hidden" name="q" value="', $purl,'">
                <input type="hidden" name="page" value="', $page-1 ,'">
                ';
            }
                echo '<button type="submit">⬅ Back</button>';
                if(!isset($_COOKIE['hQuery'])){echo '</a>';}
                else{echo '</form>';}
            echo '<p style="float:left;color:#8f8f8f;padding:10px;padding-top:22px;">Page: ',$page+1,'</p>';

            if(!isset($_COOKIE['hQuery'])){
                echo '<a href="/?q=',htmlspecialchars($purl, ENT_QUOTES | ENT_HTML5, 'UTF-8'),'&page=',$page+1,'">';
                }
                else{
                    echo '<form method="POST" action="" style="display:inline;">
                    <input type="hidden" name="q" value="', $purl,'">
                    <input type="hidden" name="page" value="', $page+1 ,'">
                    ';
                }
                echo'<button type="submit">Next ⮕</button>';
                if(!isset($_COOKIE['hQuery'])){echo '</a>';}
                else{echo '</form>';}
                echo '</div>
            <style>@media (max-width: 890px) {.nextPage {width:calc(50% + 115px);}}</style>';
        }
        else{
            echo '<div class="nextPage" style="margin-left: calc(9vw + (clamp(552px, 675px, 90vw) - 210px) / 2);">';
            if(!isset($_COOKIE['hQuery'])){
                echo '<a href="/?q=',htmlspecialchars($purl, ENT_QUOTES | ENT_HTML5, 'UTF-8'),'&page=',$page+1,'">';
                }
                else{
                    echo '<form method="POST" action="">
                    <input type="hidden" name="q" value="', $purl,'">
                    <input type="hidden" name="page" value="', $page+1 ,'">
                    ';
                }    
                echo '<button type="submit">Next ⮕</button>';

                if(!isset($_COOKIE['hQuery'])){echo '</a>';}
                else{echo '</form>';}
            echo '</div>
            <style>@media (max-width: 890px) {.nextPage {width:calc(50% + 40px);}}</style>';
        }
    }
    
    $loaded = array_fill(0, 6, false);

    if($page > 0){
        $results = qwant(null, $loaded, $Bpurl, $page);
        foreach($results as &$rs){
            echo $rs;
        }
        nextPage($purl, $page);
        return;
    }

    ##
    #Printing
    ##
    //Addons
    if(isset($_COOKIE['hQuery'])){$hideQueryCopy = hideQuery($Bpurl);}
    else{$hideQueryCopy = null;}
    if(isset($Wiki) && isset($wikiOther)){$wiki = wiki($Wiki, $wikiOther, $lang, $ddgObj, $ImpProfiles ?? null, $hideQueryCopy); $simImg = $wiki[1]; $wiki = $wiki[0];}else{$simImg = ''; $wiki = '';}
    if(isset($EtsyObj)){$etsy = etsy($EtsyObj);}
    else{$etsy='';}
    $reddit = search_reddit($redditObj);
    $related = related($related, $simImg);
    $elseWhere = '<div class="findelsewhere"><p style="float:left;">Search with </p>
    <div style="min-width:340px; float:left;">
    <a href="https://startpage.com/do/metasearch.pl?query='.$purl.'"><button>Startpage</button></a>
    <a href="https://duckduckgo.com/?q='.$purl.'"><button>DuckDuckGo</button></a>
    <a href="https://search.brave.com/search?q='.$purl.'"><button>Brave</button></a>
    <a href="https://www.mojeek.com/search?q='.$purl.'"><button>Mojeek</button></a>
        </div>
    </div>';

    //Check which addons loaded
    if($wiki != ''){$loaded[0] = true;}
    if($etsy != ''){$loaded[1] = true;}
    if($reddit != ''){$loaded[2] = true;}
    if($related != ''){$loaded[3] = true;}

    //Turn of Google if rate limite
    if(str_starts_with(json_encode($obj), '{"error":{"code":429,')){
        if(date('H') >= 7){
        file_put_contents('disGoogle.txt',date('d'));
        }
        else{
            file_put_contents('disGoogle.txt',date('d')-1);
        }
    }

    //Engines
    if($obj != ''){$results = google($obj, $loaded);}
    if(!isset($results[0]) && isset($g2obj)){$results = google2($g2obj, $loaded);}
    if(isset($BraveObj) or !isset($results[0])){$results = brave($BraveObj, $loaded, $Bpurl);}
    if(!isset($results[0])){$results = qwant($QWantObj, $loaded, $Bpurl, $page);}

    //Print
    if(isset($results))
    {
    echo $results[0],
    $wiki,
    $results[1], $results[2],$results[3],
    $reddit,
    $results[4], $results[5], $results[6],
    $related;
    if(count($results)>6){
        for ($i = 7; $i < count($results); $i++) {
            echo $results[$i];
          }
    }
    echo $elseWhere;

    if(!$dev){
    $priecoTime = microtime(true);
    $priecoResults = prieco($purl, $conn, $loc, $lang);
    $priecoTime = microtime(true) - $priecoTime;
    for($i = 0; $i < 10; $i++){
        echo $priecoResults[$i];
    }
}
    nextPage($purl,$page);
}
else{
    echo '<div style="width: 100vw;
    height: 50vh;
    display: flex;
    align-content: center;
    flex-wrap: wrap;
    justify-content: center;
    align-items: center;
    filter:brightness(0)invert(0.5);"><h1>No results found!</h1><img src="/View/icon/no_link.svg"
    style="width:100px;height:auto;"></div>';
}
}
if ($type === 'image') {

     //Next page
     function nextPage($purl,$page, $size, $color, $type, $time, $right){
        if($page > 0){
            echo '<div class="nextPage" style="width: 100vw;display: flex;justify-content: center;"><a href="/?image&imgsize=' , $size , '&imgcolor=' , $color , '&imgtype=' , $type , '&imgtime=' , $time , '&imglicence=' , $right , '&q=' , htmlspecialchars($purl, ENT_QUOTES | ENT_HTML5, 'UTF-8'),'&page=',$page-1,'"><button>⬅ Back</button></a>
            <p style="float:left;color:#8f8f8f;padding:10px;padding-top:22px;">Page: ',$page+1,'</p>
            <a href="/?image&imgsize=' , $size , '&imgcolor=' , $color , '&imgtype=' , $type , '&imgtime=' , $time , '&imglicence=' , $right , '&q=' , htmlspecialchars($purl, ENT_QUOTES | ENT_HTML5, 'UTF-8'),'&page=',$page+1,'"><button>Next ⮕</button></a></div>
            <style>@media (max-width: 890px) {.nextPage {width:calc(50% + 115px);}}</style>';
        }
        else{
            echo '
            <div class="nextPage" style="width: 100vw;display: flex;justify-content: center;"><a href="/?image&imgsize=' , $size , '&imgcolor=' , $color , '&imgtype=' , $type , '&imgtime=' , $time , '&imglicence=' , $right , '&q=' , htmlspecialchars($purl, ENT_QUOTES | ENT_HTML5, 'UTF-8'),'&page=',$page+1,'"><button>Next ⮕</button></a></div>
            <style>@media (max-width: 890px) {.nextPage {width:calc(50% + 40px);}}</style>';
        }
    }

    if ($pixabay) {
        echo '<form method="post" action="">
        <input type="submit" style="margin-left:9vw;"class="imgtoolsOption" value="Back" name="imgback" />
        </form>';
        $Pix = curl_init();
        curl_setopt($Pix, CURLOPT_URL, $PixabayFile);
        curl_setopt($Pix, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.13 (KHTML, like Gecko) Chrome/0.A.B.C Safari/525.13');
        curl_setopt($Pix, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($Pix, CURLOPT_RETURNTRANSFER, true);

        $PixObj = json_decode(curl_exec($Pix), true);
        curl_close($Pix);

        echo '
        <div style="float:left;">
         <div tabindex="0" class="imgoutbtn">
             <img alt="‎"src ="/View/img/pix.svg"class="imgout"><p style="color:black;padding-left: 10px;padding-right: 10px;">
         </div>
 
        
         <div class="bigimgout">           
                 <img src ="/View/img/pix.svg">
                 <br>
                 <h3>Thank you Pixabay for providing PriEco with these images</h3><br>
                 <p>From website: https://pixabay.com/</p><br>
                 <div class="bigimgbtn"><a href="https://pixabay.com/"><button class="imgtoolsOption">Go to website</button></a><br>
                 <a href="https://pixabay.com/static/img/logo.svg"> <button class="imgtoolsOption">Go to image</button></a></div>  
                 <div style="display: flex;justify-content: center;"><button class="bigimgclose imgtoolsOption">«Close</button></div>                      
         </div>
       </div>
         ';

        foreach ($PixObj['hits'] as &$item) {
            echo '
           <div class="imgoutdiv">
           <div tabindex="0"  class="imgoutbtn">
               <img src ="/Controller/functions/proxy.php?q=', $item['webformatURL'], '"class="imgout">
               </div>
               <a href="', $item['pageURL'], '" class="imgoutlink">
               <p style="font-size:12px;font-weight:bold;">', substr($item['user'], 0, 20) . '...</p>
               <p style="font-size:10px;">', substr($item['pageURL'], 0, 20) . '...</p>
           </a>
           
           
   
           <div class="bigimgout">           
           <img src ="/Controller/functions/proxy.php?q=', $item['largeImageURL'], '">
           <br>
           <h3>From user: ', $item['user'], '</h3><br>
           <p>From website: ', $item['largeImageURL'], '</p><br>
           <div class="bigimgbtn"><a href="', $item['pageURL'], '"><button class="imgtoolsOption">Go to website</button></a><br>
           <a href="', $item['largeImageURL'], '"> <button class="imgtoolsOption">Go to image</button></a></div>         
           <div style="display: flex;justify-content: center;"><button class="bigimgclose imgtoolsOption">«Close</button></div>         
           </div>      
         </div>
             ';
        }
        return;
    }

    include './Model/imgset.php';

    $Qimg = '{"status":"error","data":{"error_code":24}}';
    if(!file_exists('disImg.txt')){
    $BCurl = curl_init();
    curl_setopt($BCurl, CURLOPT_URL, 'https://api.qwant.com/v3/search/images/?count=75&offset='. $page*82 .'&uiv=1&locale=en_US&size=' . $imgsize . '&color=' . $imgcolor . '&imagetype=' . $imgtype . '&freshness=' . $imgtime . '&license=' . $imgright . '&q=' .$Bpurl);
    curl_setopt($BCurl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.13 (KHTML, like Gecko) Chrome/0.A.B.C Safari/525.13');
    curl_setopt($BCurl, CURLOPT_CONNECTTIMEOUT, 5);
    curl_setopt($BCurl, CURLOPT_RETURNTRANSFER, true);

    $mh = curl_multi_init();

    curl_multi_add_handle($mh, $BCurl);

    $active = null;

    do {
        curl_multi_exec($mh, $active);
    } while ($active);

    $Qimg = curl_multi_getcontent($BCurl);

    curl_multi_remove_handle($mh, $BCurl);
    curl_multi_close($mh);

    curl_close($BCurl);
}

    if($Qimg == '{"status":"error","data":{"error_code":24}}' or $Qimg == '{"status":"error","data":{"error_code":20}}'){
        if(!file_exists('disImg.txt')){
            file_put_contents('disImg.txt',date('d').' '.date('H'));
        }        

$BCurl = curl_init();
curl_setopt($BCurl, CURLOPT_URL, 'https://librex.jojoyou3.repl.co/qwant.php?offset='. $page*82 .'&imgsize=' . $imgsize . '&imgcolor=' . $imgcolor . '&imgtype=' . $imgtype . '&imgtime=' . $imgtime . '&imgright=' . $imgright . '&q=' .$Bpurl);
curl_setopt($BCurl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.13 (KHTML, like Gecko) Chrome/0.A.B.C Safari/525.13');
curl_setopt($BCurl, CURLOPT_CONNECTTIMEOUT, 5);
curl_setopt($BCurl, CURLOPT_RETURNTRANSFER, true);

$mh = curl_multi_init();

curl_multi_add_handle($mh, $BCurl);

$active = null;

do {
    curl_multi_exec($mh, $active);
} while ($active);

$Qimg = curl_multi_getcontent($BCurl);

curl_multi_remove_handle($mh, $BCurl);
curl_multi_close($mh);

curl_close($BCurl);
    }
$Qimg = json_decode($Qimg, true);
echo '<div style="margin-top:20px;"><br>';
    foreach ($Qimg['data']['result']['items'] as &$item) {
        if (!isset($item['media']) or !isset($item['media_preview'])) {
            continue;
        }

        echo '
       <div class="imgoutdiv" style="max-width:',$item['thumb_width'],'px;max-height:',$item['thumb_height'],'px;">
        <div tabindex="0" class="imgoutbtn">
            <img alt="ㅤ"src ="/Controller/functions/proxy.php?q=',$item['media_preview'], '" class="imgout" loading="lazy" decoding="async">
            </div>
        
        

        <div class="bigimgout">           
        <img src ="/Controller/functions/proxy.php?q=',$item['media_preview'], '">
        <br>
        <h3>', $item['title'], '</h3><br>
        <p>From website: ', $item['url'], '</p><br>
        <div class="bigimgbtn"><a href="', $item['url'], '"><button class="imgtoolsOption">Go to website</button></a><br>
        <a href="',$item['media'], '"> <button class="imgtoolsOption">Go to image</button></a></div>  
        <div style="display: flex;justify-content: center;"><button class="bigimgclose imgtoolsOption">«Close</button></div>   
        </div>      
      </div>
        ';
    }
    echo '</div>';
    echo nextPage($purl, $page, $imgsize, $imgcolor,$imgtype, $imgtime, $imgright);
}
if ($type == 'video') {
    if (!$dev) {
        $YoutubeCurl = curl_init();
        curl_setopt($YoutubeCurl, CURLOPT_URL, $YoutubeFile);
        curl_setopt($YoutubeCurl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.13 (KHTML, like Gecko) Chrome/0.A.B.C Safari/525.13');
        curl_setopt($YoutubeCurl, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($YoutubeCurl, CURLOPT_RETURNTRANSFER, true);
        $YoutubeObj = json_decode(curl_exec($YoutubeCurl), true);
        curl_close($YoutubeCurl);
    } else {
        $YoutubeObj =  json_decode(file_get_contents($YoutubeFile), true);
    }
    foreach ($YoutubeObj['items'] as &$item) {       
        echo '

                    <div class="imgoutdiv">
                    <a href="https://www.youtube.com/watch?v=' . $item['id']['videoId'] . '"'; if (isset($_COOKIE['new'])) {
                        echo 'target="_blank"';
                    }
                    echo'>
                    <button class="imgoutbtn">
            <img alt="ㅤ"src ="/Controller/functions/proxy.php?q=',  $item['snippet']['thumbnails']['medium']['url'], '"class="imgout" style="border-radius:20px;"loading="lazy">
            </button>
            <div class="imgoutlink">
            <p style="font-size:12px;font-weight:bold;">',  substr($item['snippet']['title'], 0, 20) . '...</p>
        <p style="font-size:10px;">', substr($item['snippet']['description'], 0, 20) . '...</p>
        </div>
        </a>
        </div>
              ';
    }
}

if ($type == 'news') {
    if (!$dev) {
        $NewsCurl = curl_init();
        curl_setopt($NewsCurl, CURLOPT_URL, $NewsFile);
        curl_setopt($NewsCurl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.13 (KHTML, like Gecko) Chrome/0.A.B.C Safari/525.13');
        curl_setopt($NewsCurl, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($NewsCurl, CURLOPT_RETURNTRANSFER, true);
        $NewsObj = json_decode(curl_exec($NewsCurl), true);
        curl_close($NewsCurl);
    } else {
        $NewsObj =  json_decode(file_get_contents($NewsFile), true);
    }
    $did = false;
    foreach ($NewsObj['articles'] as &$news) {
        $title = str_replace('<', '', $news['title']);
        $title = str_replace('>', '', $title);
        $desc = str_replace('<', '', $news['description']);
        $desc = str_replace('>', '', $desc);
        echo '<div class="output"';
        if (!$did) {
            echo 'style="border-top-left-radius: 20px;
            border-top-right-radius: 20px;"';
            $did = true;
        }
        echo ' id="output">';
        if ($news['urlToImage'] != '' and !isset($_COOKIE['datasave'])) {
            echo '<img loading="lazy" alt="‎" src="/Controller/functions/proxy.php?q=', $news['urlToImage'], '" class="OutSideImg">';
        }
        echo '<a ';
        if (isset($_COOKIE['new'])) {
            echo 'target="_blank"';
        }
        echo 'href="', $news['url'], '">
    <p style="color: gray;
    font-size: 14px;">', $news['source']['name'], '</p>
    <p class="OutTitle" style="font-size:16px;padding-top:0;padding-bottom: 5px;">', $title, '</p></a>
    <p class="snippet"style="font-size:12px;padding-bottom: 18px;">--', $desc, '</p>
    ';
        if ($providers == 'on') {
            echo '<p class="resProvider">Newsapi</p>';
        }
        echo '</div>';
    }
}
